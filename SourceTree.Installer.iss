#ifndef SourceDirectory
  #define SourceDirectory "[Output]"
#endif

#define AppName "SourceTree"
#define AppPublisher "Atlassian"
#define AppURL "https://www.sourcetreeapp.com/"
#define AppExeName "SourceTree.exe"

#define BuildVersion GetStringFileInfo(SourceDirectory + "\" + AppExeName, "ProductVersion")

[Setup]
AppId={{C08272C7-F101-47F4-B759-1AC1BCEEBA13}
AppName={#AppName}
AppVersion={#BuildVersion}
AppVerName={#AppName} {#BuildVersion}
AppPublisher={#AppPublisher}
AppPublisherURL={#AppURL}
AppSupportURL={#AppURL}
AppUpdatesURL={#AppURL}
DefaultDirName={pf}\{#AppName}
DisableProgramGroupPage=yes
DisableWelcomePage=no
DisableDirPage=no
Compression=lzma
SolidCompression=yes
OutputBaseFilename={#AppName}-setup-v{#BuildVersion}
UninstallDisplayIcon={app}\{#AppExeName}
OutputDir=[Installer]

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "{#SourceDirectory}\{#AppExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#SourceDirectory}\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{commonprograms}\{#AppName}"; Filename: "{app}\{#AppExeName}"
Name: "{commondesktop}\{#AppName}"; Filename: "{app}\{#AppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#AppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(AppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

